<!DOCTYPE html>
<html>

<head>
    <title>terms</title>
    <!-- <link rel="stylesheet" href="terms.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <link rel="stylesheet" href="{{URL::asset('public/af/css/style.css')}}">
    <link rel="stylesheet" href="{{URL::asset('public/af/assets/templates/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('public/af/assets/templates/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('public/af/assets/templates/css/style.css')}}">
    <link rel="shortcut icon" href="https://www.pngmart.com/files/8/Auction-PNG-Free-Image.png" type="image/x-icon">
    </head>
<body>
    <div class="site-navigation">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="navbar navbar-expand-lg navigation-area">
                        <!-- Site Branding -->
                        <div class="site-branding">
                       <a class="site-logo" href="{{URL('/')}}">
                                    <img src="{{asset('public/images/l.png')}}" width="240px" height="90px"
                                        style="padding: 20px" alt="Site Logo">
                                </a>
                            </a>
                        </div><!-- /.site-branding -->
                        <div class="mainmenu-area">
                                             <nav class="menu" style="display: block;">
                                    <ul id="nav">
                                        <li><a href="{{URL('/')}}">Home</a></li>

                                        <li><a href="{{URL('about')}}">About</a>
                                        </li>
                                        @guest
                                    <li >
                                        <a  href="{{ route('login') }}">{{ __('Login') }}</a>
                                    </li>
                                    @if (Route::has('register'))
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                        </li>
                                    @endif
                                @else
                                    <li >



                                            <a  href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                @csrf
                                            </form>

                                    </li>
                                @endguest
                                    <li><a href="{{route('Addproduct')}}">Categories</a>
                                        </li>
                                        <li><a href="{{URL('instruction')}}">Instructions</a>
                                        </li>
                                        
                                        <li><a href="{{route('contactus')}}">Contact</a></li>

                                        <!--<li class="dropdown-trigger">
                                            <a href="javascript:void(0)">Account</a>
                                            <ul class="dropdown-content">
                                                <li><a href="#">Login</a></li>
                                                <li><a href="#r">Register</a>
                                                </li>
                                            </ul>-->
                                        </li>
                                    </ul>
                                </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <div class="container">
      <div class="row">
        <div class="About">
            <div class="About-content">
                <br><br><br>
                <h2 class="title">TERMS&CONDITIONS</h2>
                <br><br><br>
                <p class="About-des">I HAVE READ AND UNDERSTAND ALL OF THE FOLLOWING TERMS AND CONDITIONS OF THE AUCTION.
    
                    I HEREBY AGREE TO BE BOUND BY THE FOLLOWING TERMS AND CONDITIONS.
    
                    I FURTHER AGREE TO BE RESPONSIBLE AND PAY FOR ALL PURCHASES.
    
    
    
                    100% Guarantee. Your satisfaction is our top priority; we strive to offer excellent Customer Service to
                    our clients. We will issue refunds on Online Auction Items, under certain conditions that have been
                    legally (established by or recognized by a court of law) found to be fraudulent, or unauthentic. Items
                    are sold with the Certificate of Authenticity as described and items will not be refunded due to
                    differing opinions of other companies, people, industry people, Authenticators and/or Authentication
                    opinions.
    
                    Bidding on Items
                    You must be a registered user to bid on items. By bidding, you are stating that you are ready and
                    willing to purchase that item for your bid amount, plus any shipping charges disclosed in the listing.
                    If you are outbid, or your bid does not meet the reserve price, you are under no obligation to buy. But
                    in all other cases, you are entering a contract to buy the item, and to deliver payment within the time
                    period specified (48 hours from auction closing). If you cannot pay please don’t bid. Your bids are
                    final and non-retractable. We will not accept e-mail bids.
    
                    Bidder Registration
                    All persons who wish to bid in the auction or want to purchase must register by creating an online
                    account. To register, the Bidder must provide all of the registration information requested, and sign
                    the Agreement by checking the ‘I AGREE’ box. By checking this box, you acknowledge that you are binding
                    yourself to this contractual agreement. Bidders must be 18 years of age or older and have a valid
                    driver’s license. Community Auctions reserves the right to refuse to register or admit any person at its
                    sole discretion. Invoices and correspondence relating to purchases will be sent in accordance with the
                    information on the bidder’s registration. Your registration will be accepted but your information will
                    be verified and your account may be disabled until the necessary information is provided to verify the
                    account..
                    Starting Bid
                    Community Auctions will predetermine the number of Item(s) in an auction. Community Auctions will set
                    the starting bid on each item. The Starting Bid is designed to encourage bidding. If an item is
                    incorrectly described online, Community Auctions reserves the right to rectify such error and email to
                    all active bidders the correct description.
    
                    Extended Bidding
                    Community Auctions uses an “extended bidding” process to close all of our auctions. After 8:00 pm CST,
                    for the current auction, the auction will continue for any lots that have multiple bids until there are
                    no bids placed on any lots in the auction in a fifteen (15) minute time frame until 9:00 pm CST. You
                    MUST place a bid on the lot in order to bid in extended bidding. Any lots with only one bid will close
                    at the beginning of extended bidding.
    
                    For example, during “extended bidding”, if Item #1 receives a bid within the initial fifteen (15) minute
                    time frame through 9:00pm CST, the auction is extended 15 minutes. All bidders are all on the same
                    playing field and are all equipped with the information required to ensure that the only reason they do
                    not win a given lot is they are not interested in the lot or the price for the lot went above their
                    threshold. The auction is not closed until all bids have been made, and when the auction is closed, the
                    high bidder on every lot is the person who is willing to pay the most for the item. This is precisely
                    what auctions are supposed to accomplish.
    
                    This system is great for bidders in that every lot goes to the bidder who is really willing to pay the
                    most for the item. Generating the maximum benefit to the winner’s charity of choice. This is a great
                    example of the reason to use an “extended bidding” closing style. This promotes higher prices, which is
                    great for charities, while at the same time allowing bidders to not get shut out, which is great for
                    bidders. Community Auctions reserves the right to end the auction at any time in extended bidding. </p>
                <br><br><br><br>
                <div class="social">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-instagram"></i></a>
                    <a href="#"><i class="fa fa-google"></i></a>
                </div>
            </div>
    
    
        </div>
      </div>
  </div>
 

<div class="container">
    <div class="row">
         <footer class="site-footer pd-t-120">
        <div class="footer-widget-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <aside class="widget widget_about">
                            <h2 class="widget-title">About Us</h2>
                            <div class="widget-content">
                                <p>Our site is one of the most popular and famous destinations for online auctions.
                                    A typical online auction on it often includes a Buy It Now price, which lets the
                                    buyer purchase
                                    the item for a set price before the listing's end date</p>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="footer-bottom d-flex flex-wrap-reverse justify-content-between align-items-center py-3">
                <div class="copyright-text text-center">
                    <div class="copyright"><p>Copyright © 2022 All Rights reserved by: <a href="{{url('/')}}">Online Auction</a>
                </div>
            </div>
        </div>
    </footer>
    </div>
</div>
   
        <script src="{{URL::asset('public/af/assets/templates/js/jquery.js')}}"></script>
        <script src="{{URL::asset('public/af/assets/templates/js/bootstrap.min.js')}}"></script>
        <script src="{{URL::asset('public/af/assets/templates/js/main.js')}}"></script>
</body>

</html>